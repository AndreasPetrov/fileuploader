using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using FileUploaderWebApi.Controllers;
using FileUploaderWebApi.Interfaces;
using FileUploaderWebApi.Models;
using Moq;
using NUnit.Framework;

namespace FileUploaderWebApiUnitTests
{

    internal class UploadsControllerTests
    {
        private UploadsController UploadsController { get;  set; }
        private string TestFolder { get; } = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public string TargetTestFolder { get; set; }
        private Mock<IStorageConfigurationService> StorageConfigurationServiceMock { get; } =
            new Mock<IStorageConfigurationService>();

        [SetUp]
        public void Setup()
        {
            TargetTestFolder = Path.Combine(TestFolder, Path.GetRandomFileName());
            if (Directory.Exists(TargetTestFolder))
            {
                foreach (var file in Directory.GetFiles(TargetTestFolder))
                {
                    File.Delete(file);
                }
                Directory.Delete(TargetTestFolder);
            }

            Directory.CreateDirectory(TargetTestFolder);
            if (!File.Exists(Path.Combine(TargetTestFolder, "Test1.txt")))
            {
                using (FileStream f = File.Create(Path.Combine(TargetTestFolder, "Test1.txt"), 1, FileOptions.None)) { };
            }
            if (!File.Exists(Path.Combine(TargetTestFolder, "Test2.txt")))
            {
                using (FileStream f = File.Create(Path.Combine(TargetTestFolder, "Test2.txt"), 1, FileOptions.None)) { };
            }
           
            StorageConfigurationServiceMock
                .Setup(x => x.GetTargetFolderPath())
                .Returns(TargetTestFolder);
        
            UploadsController = new UploadsController(StorageConfigurationServiceMock.Object);
        }


        [Test]
        public void UploadsController_GetFiles_Positive()
        {
            IEnumerable<UploadedFile> files = UploadsController.GetFiles();

            Assert.AreEqual(2, files.Count());
        }

        [Test]
        public void UploadsController_GetFiles_Negative()
        {
            StorageConfigurationServiceMock
                .Setup(x => x.GetTargetFolderPath())
                .Returns(string.Empty);

            UploadsController = new UploadsController(StorageConfigurationServiceMock.Object);

            Assert.Throws<ArgumentNullException>(() =>
            {
                IEnumerable<UploadedFile> files = UploadsController.GetFiles();
            });
        }

        [TearDown]
        public void CleanUp()
        {
            if (Directory.Exists(TargetTestFolder))
            {
                foreach (var file in Directory.GetFiles(TargetTestFolder))
                {
                    File.Delete(file);
                }
                Directory.Delete(TargetTestFolder);
            }
        }
    }
}