# fileUploader

ANGULAR2+ and Web API

Test task:
Using Web API and Angular create a simple page to upload/view files. The page is split into two parts: upload area and view area.
Upload area - single for all files. File size and file types that can be uploaded to the server must be configurable. Validation for invalid data should present.
View area - consists of one or more tables (one table per permitted file type). Must reflect: file name, file size, upload date, the user that uploaded a file.
The page should be as generic/reusable as possible.
Add at least two unit tests for back-end and two unit tests for front-end. Tests must have positive and negative scenarios

* project should not contain code that is not related to the task
** all external references must be excluded from the package with the test task project (please do NOT include Node Modules).

- WebAPI contains two urls:
https://localhost:50077/API/Uploads/GetFiles - to fetch the list of uploaded files 
https://localhost:50077/API/Uploads/GetFiles/UploadFiles - uploads file on the server with form-data

- The page is splited into two parts: upload area and view area.

- The file size and file types that can be uploaded to the server is be configurable with Config page
- At least two unit tests for back-end and two unit tests for front-end have been added for positive and negative scenarios.
- Upload area is single for all files. 
- Validation for invalid data is present.
- View area - consists of one or more tables (one table per permitted file type). Reflects: file name, file size, upload date, the user that uploaded a file.