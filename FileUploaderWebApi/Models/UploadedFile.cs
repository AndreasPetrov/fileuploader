﻿using System;

namespace FileUploaderWebApi.Models
{
    public class UploadedFile
    {
        public string FileName { get; set; }
        public long FileSize { get; set; }
        public string FileExtension { get; set; }
        public string FilePath { get; set; }
        public DateTime UploadDate { get; set; }
        public string UserUploadedFile { get; set; }
    }
}
