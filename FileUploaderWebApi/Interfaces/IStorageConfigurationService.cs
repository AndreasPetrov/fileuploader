﻿namespace FileUploaderWebApi.Interfaces
{
    public interface IStorageConfigurationService
    {
        string GetTargetFolderPath();
    }
}
