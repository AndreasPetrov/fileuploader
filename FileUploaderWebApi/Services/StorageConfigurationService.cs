﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FileUploaderWebApi.Interfaces;
using Microsoft.Extensions.Configuration;

namespace FileUploaderWebApi.Services
{
    public class StorageConfigurationService : IStorageConfigurationService
    {
        public IConfiguration Configuration { get; }

        public StorageConfigurationService(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string GetTargetFolderPath()
        {
            var storageFolderName = Configuration.GetValue<string>("StorageFolderName");
            if (string.IsNullOrEmpty(storageFolderName))
            {
                throw new ArgumentException($"The {nameof(storageFolderName)} is not defined");
            }
            return Path.Combine(Directory.GetCurrentDirectory(), storageFolderName);
        }
    }
}
