﻿using FileUploaderWebApi.Interfaces;
using FileUploaderWebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace FileUploaderWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UploadsController : ControllerBase
    {
        public IStorageConfigurationService StorageConfigurationService { get; }

        public UploadsController(IStorageConfigurationService storageConfigurationService)
        {
            StorageConfigurationService = storageConfigurationService;
        }


        [HttpGet]
        [Route("GetFiles")]
        public IEnumerable<UploadedFile> GetFiles()
        {
            var targetSavingFolder = StorageConfigurationService.GetTargetFolderPath();
            if (Directory.Exists(targetSavingFolder))
            {
                return Get(targetSavingFolder);
            }

            throw new ArgumentNullException($"The file storage folder does not exist {nameof(targetSavingFolder)}");
        }

        [HttpPost]
        [Route("UploadFiles")]
        public IActionResult UploadFiles()
        {
            try
            {   
                IFormFileCollection files = HttpContext.Request.Form.Files;
                if (!files.Any())
                {
                    return StatusCode(404, $"There was not found any file in request");
                }

                var targetSavingFolder = StorageConfigurationService.GetTargetFolderPath();

                foreach (IFormFile file in files)
                {
                    string fileName = file.FileName;
                    string targetSavingPath = Path.Combine(targetSavingFolder, fileName);
                    using (var stream = new FileStream(targetSavingPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                return StatusCode(201, $"Next set of files has been uploaded: " +
                       $"{string.Join(',', files.Select(x=>x.FileName))}");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal Server Error {ex}");
            }
        }

        private IEnumerable<UploadedFile> Get(string targetStorageFolder)
        {
            var files = Directory.GetFiles(targetStorageFolder);
            foreach (var file in files)
            {
                var fi = new FileInfo(file);
               
                yield return new UploadedFile
                {
                    FileName = Path.GetFileName(file),
                    FileExtension = Path.GetExtension(file),
                    FilePath = file,
                    FileSize = fi.Length,
                    UploadDate = fi.CreationTime,
                    //ToDo: replace with authenticated user from client (header)
                    UserUploadedFile = Environment.UserDomainName
                };
            }
        }
    }
}
