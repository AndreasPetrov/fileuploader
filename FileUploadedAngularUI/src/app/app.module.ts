import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent }   from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { UploadAreaComponent } from './components/upload-area/upload-area.component';
import { ViewAreaComponent } from './components/view-area/view-area.component';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { MainAreaComponent } from './components/main-area/main-area.component';
import { ValidationConfigComponent } from './components/validation-config/validation-config.component';
import { ValidationService } from './services/validation.service';

@NgModule({
    imports:      [ BrowserModule, HttpClientModule,MatTabsModule, MatTableModule, BrowserAnimationsModule, FormsModule,  AppRoutingModule ],
    declarations: [ AppComponent,  UploadAreaComponent, ViewAreaComponent, NavigationBarComponent, MainAreaComponent, ValidationConfigComponent ],
    bootstrap:    [ AppComponent],
    providers:    [ ValidationService ],
    exports:      [ RouterModule ]
})
export class AppModule { }