export class FileValidationModel
{
    file : File;
    sizeAllowed : Boolean;
    extensionAllowed : Boolean
}