 export class UploadedFile
{
     public fileName : string
     public fileSize : number
     public fileExtension : string
     public filePath : string
     public uploadDate : Date
     public userUploadedFile : string
}