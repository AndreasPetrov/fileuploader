export class AllowedExtension{
    public isAllowed: boolean;
    public extension: string;
}