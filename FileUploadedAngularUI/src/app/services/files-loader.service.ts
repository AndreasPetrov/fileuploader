import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

interface IFilesLoaderService {
  uploadFiles(files: File[]);
}

@Injectable({
  providedIn: 'root'
})
export class FilesLoaderService implements IFilesLoaderService {
  private uploadFilesURL = `${environment.baseURL + environment.uploadFilesURL}`;
  progress: number;

  @Output() public onUploadFinished = new EventEmitter();

  constructor(private http: HttpClient) {
  }
  
  public uploadFiles(files: File[]) {
    if(files.length === 0){
      return;
    }
    let headersFormData = new HttpHeaders();
    headersFormData.append('Content-Type', 'multipart/form-data');
    //headersFormData.append('UserUploadedFile', `${ (new ActiveXObject("WSCRIPT.Network")).UserName.toLowerCase()}`)
    headersFormData.append('Access-Control-Allow-Origin', '*');
    headersFormData.append('Access-Control-Allow-Methods', 'PUT,GET,POST,DELETE');
    headersFormData.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    let formData = new FormData();
    files.map((file, index)  => 
    {
       return formData.append('file', file, file.name);
    });
    this.http.post(this.uploadFilesURL, formData, 
    { 
        headers: headersFormData,
        observe: 'events',
        reportProgress: true,
        responseType: 'text'
    }).subscribe(event =>
      {
        if(event.type === HttpEventType.UploadProgress)
        {
          this.progress = event.loaded / event.total * 100;
        }
        else if(event.type === HttpEventType.Response)
        {
          let message = 'Files has been uploaded on the server';
          this.onUploadFinished.emit(event.body);
        }
      }, error => console.error(error));
  }
}
