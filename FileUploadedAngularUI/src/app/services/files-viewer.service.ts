import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UploadedFile } from '../models/uploaded-file';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class FilesViewerService implements OnDestroy {
  private baseUrl = environment.baseURL;
  private getFilesURL = `${this.baseUrl + environment.getFilesURL}`;
  public uploadedFiles : UploadedFile[] = [];
  private subscription: Observable<UploadedFile[]>;
  constructor(private http: HttpClient) { }
  
  getFiles(): UploadedFile[] {
    let headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'PUT,GET,POST,DELETE');
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    
    this.subscription = this.http.get<UploadedFile[]>(this.getFilesURL, {headers: headers,  observe: 'body'});
    this.subscription.subscribe(
      data =>
      {
         this.uploadedFiles = data
      },
      error => console.error(error));
    return this.uploadedFiles;
  }
  
  ngOnDestroy(): void {
    
  }
}
