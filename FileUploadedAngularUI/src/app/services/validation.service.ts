import { Injectable, OnInit } from '@angular/core';
import { AllowedExtension } from '../models/allowed-extension';

@Injectable({
  providedIn: 'root'
})

export class ValidationService {
  public defaultAllowedExtensionList : string[] =  ['jpg', 'doc', 'docx', 'txt', 'avi'] 
  public actualSizeMaxLimit : number = 1125899906842624;
  public actualSizeMinLimit : number = 0;
  public actualAllowedExtensionList : AllowedExtension[] = [];
  public isOnAllowedExtensions : boolean = true;
  public isOnSizeLimit : boolean = true;
  constructor() {
    this.populateDefulatAlllowedExtensionsList();
   }

  populateDefulatAlllowedExtensionsList(){
    this.actualAllowedExtensionList = [];
    this.defaultAllowedExtensionList.forEach((element,index) => {
      this.actualAllowedExtensionList.push(this.getNewExtension(element));
    });
  }

  getNewExtension(extension:string):AllowedExtension{
    let ext = new AllowedExtension();
      ext.extension =  extension;
      ext.isAllowed = true;
    return ext;
  }
}
