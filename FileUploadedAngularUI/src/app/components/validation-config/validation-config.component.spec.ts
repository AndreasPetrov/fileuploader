import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { } from 'jasmine'
import { ValidationConfigComponent } from './validation-config.component';

describe('ValidationConfigComponent', () => {
  let component: ValidationConfigComponent;
  let fixture: ComponentFixture<ValidationConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidationConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidationConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //Validation of initial initialization 
  it('should to be initialized correctly', ()=>{
      const allowedExtensionsFromService = component.validationService
      .actualAllowedExtensionList.map(x=>x.extension);
      const allowedExtensionsFromComponent = component
      .allowedExtensions.map(x=>x.extension);
      expect(allowedExtensionsFromComponent).toEqual(allowedExtensionsFromService);
  })
});
