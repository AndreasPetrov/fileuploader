import { Component, Pipe, OnInit, DoCheck } from '@angular/core';
import { AllowedExtension } from '../../models/allowed-extension'
import { ValidationService } from 'src/app/services/validation.service';
@Component({
  selector: 'app-validation-config',
  templateUrl: './validation-config.component.html',
  styleUrls: ['./validation-config.component.css']
})
export class ValidationConfigComponent implements OnInit, DoCheck {
  public isAllAllowed : boolean = true; 
  public currentInputExtension : string;
  public allowedExtensions: AllowedExtension[] = []
  constructor(public validationService : ValidationService) {

   }
  
  ngOnInit(): void 
  {
    this.allowedExtensions =  this.validationService.actualAllowedExtensionList;
  }

  ngDoCheck(): void {
    
  }

  checkAllToggle(state){
    this.allowedExtensions.forEach(element=>{
      return element.isAllowed = state;
    })
    this.onAllowedExtensionListChanged();
  }

  onSelect(event){
    this.currentInputExtension = event.target.value;
    event.stopPropagation();
  }

  addItem(){
    if(this.allowedExtensions.filter(el=>el.extension === this.currentInputExtension).length === 0){
      this.allowedExtensions.push(this.validationService.getNewExtension(this.currentInputExtension));
    }
    this.onAllowedExtensionListChanged();
  }

  removeItem(){
    let localInput = this.currentInputExtension;
    let result = this.allowedExtensions
       .filter(function(value){
        return value.extension !==  localInput
    });
    this.allowedExtensions = result;
    this.onAllowedExtensionListChanged();
  }

  onAllowedExtensionListChanged(){
    this.validationService.actualAllowedExtensionList = this.allowedExtensions.filter(el => {
      return el.isAllowed;
    });
  }
  updateMinValue(event){
    try
    {
      let inputCorrectedValue =  this.correct(event.target.value);
      this.validationService.actualSizeMinLimit = +inputCorrectedValue;
      event.stopPropagation();
    }
    catch(e)
    {
        console.log(e);
    }
  }

  updateMaxValue(event)
  {
    try
    {
      let inputCorrectedValue = this.correct(event.target.value);
      this.validationService.actualSizeMaxLimit = +inputCorrectedValue;
      event.stopPropagation(); 
     }
    catch(e)
    {
        console.log(e);
    }
  }

  private correct(val:string){
      return val.replace(/[^\d\s]+/gi, "").replace(",","");
  }
}
