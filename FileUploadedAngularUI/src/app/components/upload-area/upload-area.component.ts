import { Component, OnInit } from '@angular/core';
import { FilesLoaderService } from '../../services/files-loader.service'
import { ValidationService } from 'src/app/services/validation.service';
import { FileValidationModel } from 'src/app/models/file-validation-model';

@Component({
  selector: 'app-upload-area',
  templateUrl: './upload-area.component.html',
  styleUrls: ['./upload-area.component.css']
})
export class UploadAreaComponent implements OnInit {
  
  selectedFiles: File[] = [];
  validatedFiles: FileValidationModel[] = [];
  constructor(private loadService: FilesLoaderService,
              public validationService : ValidationService) { }

  ngOnInit(): void {

  }

  selectFiles(event) {
    this.selectedFiles = Array.from(event.target.files);
    this.validateFiles(this.selectedFiles);
  }

  validateFiles(files: File[]){
    this.validatedFiles = [];
    return files.forEach(file=>{
      let validatedFile = new FileValidationModel();
    
      validatedFile.file = file;

      validatedFile.sizeAllowed = !this.validationService.isOnSizeLimit 
                                || this.validationService.actualSizeMinLimit <= file.size 
                  && file.size <= this.validationService.actualSizeMaxLimit;

      const extension = file.name?.split('.')?.pop();
      validatedFile.extensionAllowed = !this.validationService.isOnAllowedExtensions 
                                     || this.validationService.actualAllowedExtensionList
        .filter(ext =>ext.isAllowed && ext.extension.toLowerCase() === extension.toLowerCase()).length > 0;

      this.validatedFiles.push(validatedFile);
    })
  }

   uploadFile()
   {
      this.loadService.uploadFiles(this.validatedFiles
        .filter(file=> file.sizeAllowed && file.extensionAllowed)
        .map(file => file.file));
   }

}
