import { Component, OnInit, DoCheck } from '@angular/core';
import { FilesViewerService } from '../../services/files-viewer.service'
import { UploadedFile } from 'src/app/models/uploaded-file';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-view-area',
  templateUrl: './view-area.component.html',
  styleUrls: ['./view-area.component.css']
})
export class ViewAreaComponent implements DoCheck {
  uploadedgroupedFiles: UploadedFile[][] = [];
  uploadedFiles: UploadedFile[] = [];
 
  constructor(public filesViewerService : FilesViewerService) 
  {
   
  }

  ngDoCheck(): void {
    this.fetchUpoadedFiles();
  }

  fetchUpoadedFiles() 
  {  
    this.uploadedFiles = this.filesViewerService.getFiles();
    
    if(this.uploadedFiles)
    {
      const uniqueExtensions = [...new Set(this.uploadedFiles.map(item => item.fileExtension.toLowerCase()))];
      this.uploadedgroupedFiles = [];
      uniqueExtensions.forEach((element, index) => {
        return this.uploadedgroupedFiles.push(this.uploadedFiles.filter(x=>x.fileExtension.toLowerCase() === element.toLowerCase()));
      });
    }
  }
}
