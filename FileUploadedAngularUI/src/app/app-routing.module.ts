import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadAreaComponent } from './components/upload-area/upload-area.component';
import { ViewAreaComponent } from './components/view-area/view-area.component';
import { MainAreaComponent } from './components/main-area/main-area.component';
import { AppComponent } from './app.component';
import { ValidationConfigComponent } from './components/validation-config/validation-config.component';

const routes: Routes = [
    { path: 'upload-area',  component: UploadAreaComponent },
    { path: 'view-area',  component: ViewAreaComponent },
    { path: 'main-area',  component: MainAreaComponent },
    { path: 'validation-config',  component: ValidationConfigComponent },
    { path: '', component: MainAreaComponent},
    { path: '**', component: MainAreaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  name = '';
}
