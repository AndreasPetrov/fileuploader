export const environment = {
  production: false,
  baseURL: 'https://localhost:50077/api',
  getFilesURL: '/Uploads/GetFiles',
  uploadFilesURL: '/Uploads/UploadFiles'
};

