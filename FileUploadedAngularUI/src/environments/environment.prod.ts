export const environment = {
  production: true,
  baseURL: 'https://localhost:50077/api',
  getFiles_URL: '/Upload/Files',
  uploadFiles_URL: '/Upload/UploadFiles'
};
